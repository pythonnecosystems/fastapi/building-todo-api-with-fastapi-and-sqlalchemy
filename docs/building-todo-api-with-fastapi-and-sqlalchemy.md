# FastAPI와 SQLAlchemy로 TODO API 구축: 단계별 가이드

## 소개
Python으로 API를 구축하기 위한 최첨단 웹 프레임워크인 FastAPI는 속도, 단순성 및 자동 문서화 기능으로 API 개발 세계에 혁신을 가져왔다. 이 심층 가이드에서는 FastAPI를 사용하여 강력한 TODO API를 만드는 과정을 안내한다. FastAPI는 Python 타입 힌트, 자동 데이터 유효성 검사, OpenAPI 및 JSON 스키마 문서 생성 기능을 지원하므로 효율적이고 문서화된 API를 구축하려는 개발자에게 탁월한 선택이 될 수 있다.

API 개발을 처음 접하는 초보자이든, 최신 고성능 프레임워크를 찾는 숙련된 개발자이든, FastAPI는 모든 개발자에게 도움을 줄 수 있을 것이다. TODO API를 구축하는 단계에 대해 자세히 알아보고 FastAPI의 기능을 활용하여 강력하고 확장 가능한 솔루션을 만들어 보자.

## 전제조건
FastAPI로 TODO API 구축을 시작하기 전에 시스템에 다음 필수 구성 요소가 설치되어 있는지 확인하여야 한다.

- **Python 3.7 이상**: FastAPI는 Python 3.7 이상 버전이 필요하다. Python은 [공식 Python 웹사이트](https://www.python.org/downloads/)에서 다운로드받을 수 있다.
- **pip(Python 패키지 인스톨러)**: **pip**은 FastAPI와 기타 종속성을 설치하는 데 사용되므로 반드시 설치해야 한다.

## 프로젝트 설정

### 프로젝트 디렉토리 생성
터미널을 열고 FastAPI 프로젝트를 위한 새 디렉터리를 만든다.

```bash
$ mkdir fasrapi_todo
$ cd fastapi_todo
```

### 가상 환경 설정
프로젝트 디렉터리에서 가상 환경을 만든다. 이 단계는 프로젝트의 종속성을 격리하는 데 필수적이다.

```bash
$ python -m venv venv
```

### 가상 환경 활성화
운영 체제에 따라 가상 환경을 활성화한다.

- MacOS와 Linux
 
```bash
$ source venv/bin/activate
```

### FastAPI와 Uvicorn 설치

#### FastAPI

[FastAPI](https://fastapi.tiangolo.com/)는 표준 Python 타입 힌트를 기반으로 Python 3.7 이상에서 API를 구축하기 위한 현대적이고 빠른 웹 프레임워크이다. 사용하기 쉽고 고성능을 제공하도록 설계되어 API 개발에 탁월한 선택이 될 것이다.

```bash
(venv) $ pip install fastapi
```

이 명령은 `pip`에게 FastAPI 패키지를 종속 요소와 함께 다운로드하여 설치하도록 지시한다. 설치가 완료되면 FastAPI의 기능을 활용하여 강력한 API를 만들 수 있다.

#### Uvicorn

[Uvicorn](https://www.uvicorn.org/)은 초고속 ASGI(비동기식 서버 게이트웨이 인터페이스) 서버로, FastAPI 어플리케이션을 실행하는 데 이상적이다.

```bash
(venv) $ pip install uvicorn
```

Uvicorn을 설치하면 FastAPI 어플리케이션을 실행할 수 있는 고성능 서버를 확보할 수 있다. Uvicorn은 비동기 요청을 효율적으로 처리할 수 있으며, 이는 비동기 작업을 처리하는 FastAPI의 기능에 부합한다.

#### Pydantic

Pydantic은 Python용 데이터 유효성 검사와 설정 관리를 위한 라이브러리이다. 데이터 모델을 정의하고 유효성을 검사하기 위해 FastAPI와 같은 프레임워크와 함께 널리 사용된다.

Pydantic의 주요 기능은 다음과 같다.

- 데이터 유효성 검사: Pydantic을 사용하면 Python 유형 힌트를 사용하여 데이터 모델을 정의할 수 있다. 이러한 모델에 대해 입력 데이터의 유효성을 자동으로 검사하여 지정된 구조와 데이터 타입을 준수하는지 확인한다.
- 설정 관리: Pydantic은 유효성 검사를 넘어 어플리케이션 설정 관리에도 사용된다. 어플리케이션의 구성을 나타내는 Pydantic 모델을 정의하여 환경 변수, 파일 또는 기타 소스에서 설정을 쉽게 로드할 수 있다.
- FastAPI 통합: Pydantic은 자동 요청과 응답 유효성 검사를 제공함으로써 FastAPI에서 중심적인 역할을 한다. FastAPI에서 Pydantic 모델은 들어오는 요청 데이터와 나가는 응답 데이터의 구조를 정의하는 데 사용된다. 이 통합은 깔끔하고 타입이 안전한 코드를 촉진한다.
- 타입 검사: Pydantic은 Python의 타입 힌트 시스템을 활용한다. 이는 문서화 목적뿐만 아니라 정적 타입 검사 도구가 개발 초기에 잠재적인 문제를 포착할 수 있도록 한다.
- 간결성과 가독성: Pydantic 모델은 간결하고 가독성이 뛰어나서 데이터의 예상 구조를 쉽게 이해할 수 있다. 이러한 단순성은 코드 유지관리와 협업을 향상시킨다.

```bash
(venv) $ pip install pydantic pydantic-settings
```

가상 환경 내에서 이러한 `pip` 명령을 실행하면 FastAPI 프로젝트의 종속성을 격리할 수 있다. 이렇게 하면 프로젝트의 종속성이 시스템의 다른 Python 프로젝트를 방해하지 않도록 할 수 있다. FastAPI와 Uvicorn이 설치되었으므로 이제 FastAPI의 비동기 프레임워크의 강점을 사용하여 TODO API를 구축할 준비가 되었다.

## 프로젝트 폴더 구조
이제 필수적인 데이터베이스 구성을 설정했으므로 이제 명확한 폴더 구조로 FastAPI 프로젝트를 정리해 보겠다. 잘 구조화된 프로젝트는 코드 가독성, 확장성 및 협업의 용이성을 유지하는 데 도움을 준다. 아래는 제안된 폴더 구조이다.

```
.
├── .env
├── .git
├── .gitignore
├── LICENSE
├── README.md
├── app
│   ├── __init__.py
│   ├── api
│   │   ├── __init__.py
│   │   └── v1
│   │       ├── __init__.py
│   │       └── todos
│   │           ├── __init__.py
│   │           ├── create.py
│   │           ├── delete.py
│   │           ├── read.py
│   │           └── update.py
│   ├── core
│   │   ├── __init__.py
│   │   └── config.py
│   ├── database
│   │   └── __init__.py
│   ├── main.py
│   ├── models
│   │   └── __init__.py
│   ├── schemas
│   │   └── __init__.py
│   └── tests
│       └── __init__.py
└── requirements.txt
```

폴더 구조는 모듈식으로 체계적으로 설계된 FastAPI 프로젝트를 나타낸다. 각 구성 요소를 자세히 살펴보자.

- **.env**: 이 파일에는 일반적으로 애플리케이션의 환경 변수, 구성 설정 또는 비밀 번호를 포함한다. 이 파일은 어플리케이션에 이러한 변수를 로드하기 위해 `python-dotenv`와 같은 라이브러리와 함께 사용되는 경우가 많다.
- **.git**: 이 폴더에는 Git 버전 관리에 필요한 모든 파일이 들어 있다. 이 폴더는 코드베이스의 변경 사항을 추적하고, 공동 작업을 용이하게 하며, 필요한 경우 이전 버전으로 되돌릴 수 있게 해준다.
- **.gitignore**: 이 파일은 Git에서 무시해야 하는 파일과 디렉터리를 나열한다. 일반적으로 임시 파일, 로그 및 가상 환경 디렉터리와 같은 파일이 포함된다.
- **LICENSE**: 이 파일에는 다른 사람이 코드를 사용, 수정 및 배포할 수 있는 방법을 나타내는 프로젝트의 라이선스 정보가 포함되어 있다.
- **README.md**: 이 파일은 일반적인 프로젝트의 기본 문서 파일이다. 개발자 또는 사용자를 위한 개요, 설치 지침 및 기타 관련 정보를 제공한다.
- **app**: FastAPI 어플리케이션이 들어 있는 main 폴더이다.
- **app/\_\_init__.py**: 이 파일은 `app` 폴더를 Python 패키지로 만든다.
- **app/api**: 이 폴더에는 API 관련 모듈이 들어 있다.
- **app/api/\_\_init__.py**: `api` 폴더를 Python 패키지로 만든다.
- **app/api/v1**: API 버전을 나타낸다. 프로젝트가 발전함에 따라 다양한 API 버전이 포함될 수 있다.
- **app/api/v1/\_\_init__.py**: `v1` 폴더를 Python 패키지로 만든다.
- **app/api/v1/todos**: 할 일과 관련된 특정 API 엔드포인트를 나타낸다.
- **app/api/v1/todos/\_\_init__.py**: `todos` 폴더를 Python 패키지로 만든다.
- **app/api/v1/todos/create.py**, **app/api/v1/todos/delete.py**, **app/api/v1/todos/read.py**, **app/api/v1/todos/update.py**: 이러한 파일에는 todos API 엔드포인트에 대한 CRUD(생성, 읽기, 업데이트, 삭제) 작업이 포함되어 있을 가능성이 높다.
- **app/core**: 이 폴더에는 어플리케이션의 핵심 구성과 설정이 들어 있다.
- **app/core/\_\_init__.py**: `core` 폴더를 Python 패키지로 만든다.
- **app/core/config.py**: 데이터베이스 구성 또는 API 설정과 같은 FastAPI 앱의 구성 설정이 들어 있다.
- **app/database**: 이 폴더는 데이터베이스 관련 파일이나 구성을 저장하는 데 사용할 수 있다.
- **app/database/\_\_init__.py**: `database` 폴더를 Python 패키지로 만든다.
- **app/main.py**: FastAPI 어플리케이션의 엔트리 포인트이다. 일반적으로 FastAPI 앱 인스턴스와 기타 설정을 포함한다.
- **app/models**: 어플리케이션의 데이터 모델을 포함한다.
- **app/schemas**: 데이터 유효성 검사에 사용되는 Pydantic 스키마가 포함되어 있다.
- **app/tests**: 테스트 파일을 저장한다.
- **requirements.txt**: 프로젝트에 필요한 Python 패키지와 해당 버전을 나열한다. 이 파일은 종속성을 설치하기 위해 `pip`과 함께 자주 사용된다.

이 구조는 모듈식 설계를 촉진하여 FastAPI 어플리케이션을 더 쉽게 구성, 유지 관리 및 확장할 수 있도록 한다. 각 구성 요소에는 고유한 책임이 있어 코드베이스의 전반적인 구성과 가독성에 기여한다.

## Docker로 MySQL 데이터베이스 배포
FastAPI 어플리케이션을 구축하기 전에 Docker를 사용하여 MySQL 데이터베이스를 설정해 보겠다. 이렇게 하면 개발을 위한 편리하고 격리된 환경을 제공할 수 있다.

```bash
$ docker run --name fastapi_todo -d \
    -p 3306:3306 \
    -e MYSQL_ROOT_PASSWORD=fastapi_todo \
    -e MYSQL_USER=fastapi_todo \
    -e MYSQL_PASSWORD=fastapi_todo \
    -e MYSQL_DATABASE=fastapi_todo \
    mysql:latest
```

다음은 옵션에 대한 설명이다.

- ` --name fastapi_todo`: Docker 컨테이너의 이름을 "fastapi_todo"로 지정한다.
- `-d`: 컨테이너를 백그라운드에서 실행한다(분리 모드).
- `-p 33066:3306`: 컨테이너의 포트 3306을 호스트 머신의 포트 33066에 매핑한다.
- `-e MYSQL_ROOT_PASSWORD=fastapi_todo`: MySQL의 루트 비밀번호를 설정한다.
- `-e MYSQL_USER=fastapi_todo`: "fastapi_todo"라는 이름의 MySQL 사용자를 생성한다.
- `-e MYSQL_PASSWORD=fastapi_todo`: MySQL 사용자의 비밀번호를 설정한다.
- `-e MYSQL_DATABASE=fastapi_todo`: "fastapi_todo"라는 이름의 MySQL 데이터베이스를 만든다.
- `mysql:latest`: 사용할 Docker 이미지를 지정한다(이 경우 Docker Hub의 최신 버전의 MySQL 이미지).

이제 FastAPI 어플리케이션에서 사용할 수 있는 MySQL 서버가 준비되었다. 다음 섹션에서는 이 MySQL 데이터베이스와 상호 작용하도록 FastAPI 앱을 구성하겠다. 시작하자!

## SQLAlchemy에 대한 간단한 소개
SQLAlchemy는 널리 사용되는 Python용 SQL 툴킷이자 객체 관계형 매핑(ORM) 라이브러리이다. 관계형 데이터베이스와 상호 작용하기 위한 일련의 고급 API를 제공하여 개발자가 보다 Python적인 방식으로 데이터베이스 작업을 할 수 있도록 해준다.

이제 SQLAlchemy와 제공된 자격 증명을 사용하여 MySQL 데이터베이스에 연결해 보겠다.

### SQLAlchemy로 MySQL 데이터베이스에 연결
FastAPI 프로젝트에서는 SQLAlchemy를 사용하여 MySQL 데이터베이스와 상호 작용하려고 한다. 데이터베이스에 연결하려면 `app/database` 디렉토리 안에 `database.py`라는 파일을 만들어야 한다.

### SQLAlchemy 설치
SQLAlchemy가 설치되어 있는지 확인한다. 설치되어 있지 않은 경우 다음을 사용하여 설치할 수 있다.

```bash
(venv) $ pip install sqlalchemy mysql-connector-python
```

### `app/database/database.py` 생성
이제 `database.py` 파일을 생성하자.

```python
# app/database/database.py

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.core.config import settings


DATABASE_URL = f"mysql+mysqlconnector://{settings.DB_USER}:{settings.DB_PASSWORD}@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}"

# Creating the SQLAlchemy engine
engine = create_engine(DATABASE_URL)

# Creating the session
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Function to get a database session
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
```

코드에 대한 설명입니다:

- `DATABASE_URL`: MySQL 데이터베이스에 연결할 URL이다. 여기에는 username, password, host, port 및 database name이 포함됩니다.
- `engine`: 데이터베이스 연결을 관리하는 SQLAlchemy 엔진이다.
- `SessionLocal`: 데이터베이스 세션을 만들기 위한 클래스이다.
- `Base`: 모델의 기본 클래스이다.
- `get_db()`: 데이터베이스 세션을 반환하는 함수이다.

다음 섹션에서는 이 `database.py` 파일을 사용하여 MySQL 데이터베이스와 상호 작용할 것이다.

`core.config` 파일의 설정은 나중에 설명할 예정이다.

### `Base_class`
`app/database` 폴더에 `base_clasee.py` 파일을 생성한다.

```python
# app/database/base_class.py

from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import DeclarativeMeta


class CustomBase:
    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

Base: DeclarativeMeta = declarative_base(cls=CustomBase)
```

이제 데이터베이스 폴더에 `base_class.py`를 생성하였다. 여기에 나중에 설명할 alembic과 함께 사용할 모든 모델에 대한 엔트리포인트를 추가할 수 있다.

```python
from app.database.base_class import Base
from app.models.todo import Todo
```

## App CONFIG 파일

```python
# app/core/config.py

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    API_VERSION: str = "1.0.0"
    CONTACT: dict = {
        "name": "KtechHub FastAPI Demo",
        "url": "https://www.ktechhub.com/",
        "email": "info@ktechhub.com",
    }
    ENV: str = "dev"
    if ENV == "dev":
        RELOAD: bool = True
        LOG_LEVEL: str = "debug"
    else:
        RELOAD: bool = False
        LOG_LEVEL: str = "info"
    
    ALLOWED_HOSTS: list = ["*"]
    
    DB_USER: str
    DB_PASSWORD: str
    DB_HOST: str
    DB_PORT: int
    DB_NAME: str
    class Config:
        env_file = "./.env"
        extra = "ignore"

settings = Settings()
```

이 `config.py` 파일은 FastAPI 어플리케이션의 설정을 관리하고 유효성을 검사하는 방법인 `Settings`라는 Pydantic `BaseSettings` 클래스를 정의하고 있다. 몇 가지 주요 요소를 분석해 보겠다.

- `API_VERSION`: API의 버전이다. 이 경우 `"1.0.0"`으로 설정되어 있다.
- `CONTACT`: API와 관련된 담당자 또는 조직에 대한 정보이다.
- `ENV`: 어플리케이션이 실행되고 있는 환경이다. `"dev"`(개발)인 경우 자동 재로드와 디버그 로깅같은 특정 설정이 활성화된다.
- `RELOAD`: 개발 모드인 경우 `True`로 설정되어 코드 변경 시 서버로 자동으로 다시 로드할 수 있다.
- `LOG_LEVEL`: 어플리케이션의 로깅 수준이다. 개발 환경에서는 `debug`로 설정되고 다른 환경에서는 `info`로 설정된다.
- `ALLOWED_HOSTS`: API에 연결할 수 있는 호스트 목록이다. 이 경우 모든 호스트를 의미하는 `["*"]`로 설정된다.
- `DB_USER`, `DB_PASSWORD`, `DB_HOST`, `DB_PORT`, `DB_NAME`: 데이터베이스 연결 세부 정보. 이는 아마도 환경 변수 또는 구성 파일에 설정되어 있을 것이다.
- `Config`: 이 내부 클래스는 Pydantic 설정의 동작을 구성한다.
- `env_file`: 데이터베이스 자격 증명과 같은 민감한 정보를 포함할 수 있는 `.env` 파일의 위치를 지정한다.
- `extra`: ``.env` 파일에서 추가 필드를 처리하는 방법을 지정한다. `"ignore"`는 모든 추가 필드가 무시됨을 의미한다.
- `settings = Settings()`: `Settings` 클래스의 인스턴스가 생성되며, 이 인스턴스는 FastAPI 어플리케이션 전체에서 이러한 설정에 액세스하는 데 사용된다.

전반적으로 이 구성 파일은 FastAPI 어플리케이션에 필요한 다양한 설정을 중앙 집중화하여 관리하며 그에 대한 유지 관리가 더 쉬워진다. 설정은 환경 변수 또는 `.env` 파일에서 로드되므로 유연성과 보안을 제공한다.

`.env` 파일을 다음과 같이 업데이트한다.

```
API_VERSION=1.0.0
ENV=dev
ALLOWED_HOSTS=["*"]
DB_USER=fastapi_todo
DB_PASSWORD=fastapi_todo
DB_HOST=localhost
DB_PORT=3306
DB_NAME=fastapi_todo
```

## Todo 모델

```python
# app/models/todo.py

from sqlalchemy import Column, Integer, String, Boolean, DateTime, func
from app.database.base_class import Base

class Todo(Base):
    __tablename__ = "todos"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(120), index=True, )
    description = Column(String(255), index=True, nullable=True)
    is_completed = Column(Boolean, default=False)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now(), onupdate=func.now())
    def __str__(self):
        return f"Todo #{self.id}: {self.title}, Completed: {self.is_completed}"
```

각 필드에 대한 설명이다.

- `id`: `todos` 테이블의 주(primary) 키이다.
- `title`: to-do 항목의 제목이다.
- `description`: to-do 항목에 대한 설명이다. 모든 할 일에 설명이 필요한 것은 아니므로 null 값을 허용한다.
- `is_completed`: to-do가 완료되었는지 여부를 나타내는 부울 필드이다. 기본값은 `False`이다.
- `created_at`: to-do가 생성된 시점을 나타내는 타임스탬프이다. 기본값은 레코드가 만들어질 때 현재 타임스탬프이다.
- `updated_at`: to-do이 마지막으로 업데이트된 시간을 나타내는 타임스탬프이다. 기본적으로 레코드가 만들어질 때 현재 타임스탬프로 설정되며 레코드가 수정될 때마다 자동으로 업데이트된다.

`Base` 클래스는 `app/database/database.py`에서 가져온 것으로 SQLAlchemy 모델의 기본 클래스를 나타낸다. 이 클래스는 모든 모델이 동일한 데이터베이스 세션과 메타데이터를 공유하도록 한다.

이제 to-do 항목을 저장하기 위한 데이터베이스 테이블과 상호 작용하는 데 사용할 수 있는 `Todo` 모델이 생겼다.

## Alembic
Alembic은 SQLAlchemy 기반 프로젝트에서 데이터베이스 마이그레이션을 위한 훌륭한 도구이다. Alembic을 설치하고 FastAPI 앱에서 설정하는 방법은 다음과 같다.

### Alembic 설치
터미널 또는 명령 프롬프트에서 다음 명령을 실행하여 Alembic을 설치할 수 있다.

```bash
(venv) $ pip install alembic
```

### Alembic 구성 파일 생성
아래 명령어를 사용하여 Alembic 구성 파일을 만든다.

```bash
(venv) $ alembic init alembic
```

이렇게 하면 프로젝트의 루트 디렉터리에 `alembic`이라는 폴더가 생성된다.

alembic 폴더의 `env.py`를 업데이트한다.

```python
# alembic/env.py

from logging.config import fileConfig
from sqlalchemy import engine_from_config
from sqlalchemy import pool
from alembic import context
from app.database.database import DATABASE_URL
from app.database.base import Base

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = Base.metadata
# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.

def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.
    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.
    Calls to context.execute() here emit the given string to the
    script output.
    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )
    with context.begin_transaction():
        context.run_migrations()

def run_migrations_online() -> None:
    """Run migrations in 'online' mode.
    In this scenario we need to create an Engine
    and associate a connection with the context.
    """
    configuration = config.get_section(config.config_ini_section)
    configuration["sqlalchemy.url"] = DATABASE_URL
    connectable = engine_from_config(
        configuration,
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )
    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata
        )
        with context.begin_transaction():
            context.run_migrations()

if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
```

### 초기 마이그레이션 생성
다음 명령을 실행하여 초기 마이그레이션을 생성한다.

```bash
(venv) $ alembic revision --autogenerate -m "initial"
```

이렇게 하면 `alembic/versions` 폴더에 새 파일이 생성된다. 이 파일을 커스터마이즈하여 초기 데이터베이스 스키마를 정의할 수 있다.

### 마이그레이션에 적용
마이그레이션을 적용하고 데이터베이스에 테이블을 만들도록 실행한다.

```bash
(venv) $ $ alembic upgrade head
```

이렇게 하면 모든 마이그레이션에 적용된다.

이제 모델을 변경할 때마다 새 마이그레이션을 생성하고 Alembic을 사용하여 적용할 수 있다.

모델을 변경할 때마다 실행하기만 하면 된다.

```bash
(venv) $ $ alembic upgrade head && \
alembic revision --autogenerate && \
alembic upgrade head
```

## Todo Pydantic 스키마
API와 상호 작용할 때 데이터의 구조를 정의하는 Pydantic 스키마를 만들 수 있다. 다음은 새 todo를 만들기 위한 `TodoCreate` 스키마와 할 일을 읽고 업데이트하기 위한 `Todo` 스키마를 만드는 방법이다.

```python
# app/schemas/todo.py
from pydantic import BaseModel
from datetime import datetime
from typing import Optional

class TodoBase(BaseModel):
    title: str
    description: Optional[str] = None
    is_completed: Optional[bool] = False

class TodoCreate(TodoBase):
    pass

class TodoUpdate(TodoBase):
    pass

class Todo(TodoBase):
    id: int
    created_at: datetime
    updated_at: datetime
    class Config:
        orm_mode = True
```

이 스키마는 API가 수신(생성용)하거나 반환(읽기와 업데이트용)하는 데이터의 구조를 정의한다. `TodoCreate` 스키마는 새 todo을 만드는 데 사용되며, `Todo` 스키마는 기존 할 일을 읽고 업데이트하는 데 사용된다. `Todo` 스키마의 `orm_mode=True`를 사용하면 SQLAlchemy 모델에서 직접 사용할 수 있다.

특정 요구 사항에 따라 이러한 스키마를 자유롭게 조정할 수 있다.

## API 엔드포인트
todo API에 대해 자세히 알아보자.

### 일기 파일
`app/api/v1/todos/read.py`에서 todo를 읽기 위한 엔드포인트를 정의한다. 구조는 다음과 같다.

```python
# app/api/v1/todos/read.py

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.database.database import get_db
from app.models.todo import Todo
from app.schemas.todo import Todo as TodoSchema
from typing import List

router = APIRouter()

@router.get("/todos/", response_model=List[TodoSchema])
async def read_todos(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    todos = db.query(Todo).offset(skip).limit(limit).all()
    return todos

@router.get("/todos/{id}", response_model=TodoSchema)
async def read_todo(id: int, db: Session = Depends(get_db)):
    todo = db.query(Todo).filter(Todo.id == id).first()
    if todo is None:
        raise HTTPException(status_code=404, detail="Todo not found")
    return todo
```

위의 코드에서 두 엔드포인트를 정의한다.

**모든 Todo 목록**: `/todos/`<br>
- 이 엔드포인트는 todo 목록을 반환한다. `skip`과 `limit`같은 쿼리 매개변수를 사용하여 결과를 페이지 매김할 수 있다.

**ID로 Todo 읽기**: `/todos/{id}`<br>
- 이 엔드포인트는 제공된 `id`에 해당하는 단일 todo를 반환한다. 지정된 ID를 가진 todo가 존재하지 않으면 404 오류를 반환한다.

필요한 모듈과 종속성을 가져와서 사용해야 한다.

### 생성 파일
`app/api/v1/todos/create.py`에서 todo 생성을 위한 엔드포인트를 정의한다. 코드는 다음과 같다.

```python
# app/api/v1/todos/create.py

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.database.database import get_db
from app.schemas.todo import TodoCreate
from app.schemas.todo import Todo as TodoResponse
from app.models.todo import Todo
from datetime import datetime

router = APIRouter()

@router.post("/todos/", response_model=TodoResponse)
async def create_todo(todo: TodoCreate, db: Session = Depends(get_db)):
    """
    Create a new todo.
    """
    # You can customize this logic based on your requirements.
    db_todo = Todo(
        title=todo.title,
        description=todo.description,
        is_completed=todo.is_completed,
        created_at=datetime.utcnow(),
        updated_at=datetime.utcnow(),
    )
    db.add(db_todo)
    db.commit()
    db.refresh(db_todo)
    return db_todo
```

- `db.add(db_todo)`: 데이터베이스 세션에 새 todo를 추가한다.
- `db.commit()`: 데이터베이스에 트랜잭션을 커밋한다.
- `db.refresh(db_todo)`: 데이터베이스의 데이터로 todo를 새로 고친다.

### 업데이트 파일
`app/api/v1/todos/update.py`에서 todo 업데이트를 위한 엔드포인트를 정의한다. 코드는 다음과 같다.

```python
# app/api/v1/todos/update.py

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.database.database import get_db
from app.schemas.todo import TodoUpdate
from app.schemas.todo import Todo as TodoResponse
from app.models.todo import Todo
from datetime import datetime

router = APIRouter()

@router.put("/todos/{id}", response_model=TodoResponse)
async def update_todo(
    id: int, todo: TodoUpdate, db: Session = Depends(get_db)
):
    """
    Update a todo.
    """
    db_todo = db.query(Todo).filter(Todo.id == id).first()
    if not db_todo:
        raise HTTPException(status_code=404, detail="Todo not found")
    for field, value in todo.dict(exclude_unset=True).items():
        setattr(db_todo, field, value)
    db_todo.updated_at = datetime.utcnow()
    db.commit()
    db.refresh(db_todo)
    return db_todo
```

### 삭제 파일
`app/api/v1/todos/delete.py`에서 todo 삭제를 위한 엔드포인트를 정의한다. 코드는 다음과 같다.

```python
# app/api/v1/todos/delete.py

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.database.database import get_db
from app.models.todo import Todo

router = APIRouter()

@router.delete("/todos/{id}")
async def delete_todo(id: int, db: Session = Depends(get_db)):
    """
    Delete a todo.
    """
    db_todo = db.query(Todo).filter(Todo.id == id).first()
    if not db_todo:
        raise HTTPException(status_code=404, detail="Todo not found")
    db.delete(db_todo)
    db.commit()
    return {"message": "Todo deleted successfully"}
```

### Main Todo 파일
app/api/v1/todos/todo.py`에서 모든 apis에 대한 엔트리포인트를 정의한다.

```python
# app/api/v1/todos/todo.py
from fastapi import APIRouter
from app.api.v1.todos import read, create, update, delete

router = APIRouter()

router.include_router(read.router, prefix="", tags=["todos"])
router.include_router(create.router, prefix="", tags=["todos"])
router.include_router(update.router, prefix="", tags=["todos"])
router.include_router(delete.router, prefix="", tags=["todos"])
```

### Main 파일

```python
# app/main.py

from fastapi import FastAPI, status
from fastapi.responses import RedirectResponse
from fastapi.middleware.cors import CORSMiddleware
from app.core.config import settings
from app.api.v1.todos import todo

description_text = """
FastAPI TODO API Demo
"""
app = FastAPI(
    title="FastAPI TODO API Demo",
    openapi_url="/openapi.json",
    description=description_text,
    version=settings.API_VERSION,
    contact=settings.CONTACT,
    terms_of_service="https://www.ktechhub.com/terms/",
)
app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.ALLOWED_HOSTS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/", response_class=RedirectResponse, include_in_schema=False)
async def docs():
    return RedirectResponse(url="/docs")
@app.get("/ready", status_code=status.HTTP_200_OK, include_in_schema=True)
async def ready() -> str:
    """Check if API it's ready"""
    return "ready"

app.include_router(
    todo.router,
    prefix="/api/v1",
    tags=["todos"],
)
```

원하는 대로 커스터마이즈할 수 있다.

## 최종 폴더 구조

```
.
├── .env
├── .git
├── .gitignore
├── LICENSE
├── README.md
├── alembic
│   ├── README
│   ├── env.py
│   ├── script.py.mako
│   └── versions
│       ├── .gitkeep
│       └── a4d1ef99ffe1_initial.py
├── alembic.ini
├── app
│   ├── __init__.py
│   ├── api
│   │   ├── __init__.py
│   │   └── v1
│   │       ├── __init__.py
│   │       └── todos
│   │           ├── __init__.py
│   │           ├── create.py
│   │           ├── delete.py
│   │           ├── read.py
│   │           ├── todo.py
│   │           └── update.py
│   ├── core
│   │   ├── __init__.py
│   │   └── config.py
│   ├── database
│   │   ├── __init__.py
│   │   ├── base.py
│   │   ├── base_class.py
│   │   └── database.py
│   ├── main.py
│   ├── models
│   │   ├── __init__.py
│   │   └── todo.py
│   ├── schemas
│   │   ├── __init__.py
│   │   └── todo.py
│   └── tests
│       └── __init__.py
└── requirements.txt
```

## API 엔드포인트 테스팅
앱을 시작한다.

```bash
$ uvicorn app.main:app --reload
```

그런 다음 http://127.0.0.1:8000/docs 을 방문하여 엔드포인트를 확인한다.

![](./images/0_mLX5hhCCe5sytB_d.webp)

todo 생성

![](./images/0_dX-tk2g_nxDGijSu.webp)

todo 목록

![](./images/0_Wu78mxZ27h7M4LkW.webp)

단일 todo 항목 읽기

![](./images/0_MvLBuuScJY58ISc9.webp)

todo 항목 업데이트

![](./images/0_P9VI_ONIigSxI06Z.webp)

todo 항목 삭제

![](./images/0_PoKrJvd2sssQUNY-.webp)

## 마치며
축하합니다! FastAPI를 사용해 강력하고 효율적인 TODO API를 만드는 여정을 성공적으로 마쳤다. 프로젝트 설정, MySQL 데이터베이스 연결, 모델과 스키마 정의부터 todo 읽기, 생성, 업데이트 및 삭제를 위한 강력한 API 엔드포인트 제작에 이르기까지 모든 것을 다루었다.

FastAPI는 최신 접근 방식, 자동 OpenAPI와 JSON 스키마 문서화 및 비동기 기능을 통해 API 구축에 탁월한 선택이 될 수 있다. SQLAlchemy를 사용하면 데이터베이스 상호 작용이 향상되고, Pydantic을 사용하면 데이터 유효성 검사와 직렬화가 쉬워진다.

FastAPI의 세계는 방대하며 탐색하고 구현해야 할 것이 훨씬 더 많다는 것을 기억하자. 사용자 인증, 테스트 및 프로덕션 배포같은 기능으로 API를 개선하는 것을 고려해 보세요. FastAPI의 풍부한 문서와 커뮤니티 리소스를 살펴보고 이 뛰어난 프레임워크의 잠재력을 최대한 활용해보도록 하자.

## 참고 문헌
- 원래 게시처는 https://www.ktechhub.com/tutorials/building-a-todo-api-with-fastapi-and-sqlalchemy-a-step-by-step-guide 이다.

- 작성자 홈페이지: https://www.ktechhub.com/me/mumunimohammed

- 코드의 GitHub 리포지토리: https://github.com/ktechhub/fastapi_todo
